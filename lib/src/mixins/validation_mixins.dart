import 'package:email_validator/email_validator.dart';

class ValidationMixin {
  String? validateEmail(String? value) {
    if (value!.isEmpty) {
      return 'Please enter your email address';
    }
    if (!EmailValidator.validate(value.toString())) {
      return 'Please enter an valid email';
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value!.length < 4) {
      return 'Password must be at least 4 characters';
    }
    return null;
  }
}
